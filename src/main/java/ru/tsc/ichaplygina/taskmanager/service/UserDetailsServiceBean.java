package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.model.CustomUser;
import ru.tsc.ichaplygina.taskmanager.model.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        @Nullable final User user = userService.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        @NotNull final List<Role> userRoles = user.getRoles();
        System.out.println("DEBUG666: " + userRoles.size());
        for (Role role : userRoles) {
            System.out.println("DEBUG666: " + role.toString());
        }
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final Role role : userRoles) roles.add(role.toString());

        org.springframework.security.core.userdetails.User.UserBuilder builder =
                org.springframework.security.core.userdetails.User
                        .withUsername(username)
                        .password(user.getPasswordHash())
                        .roles(roles.toArray(new String[]{}));

        final UserDetails details = builder.build();
        org.springframework.security.core.userdetails.User result = (org.springframework.security.core.userdetails.User) details;
        final CustomUser customUser = new CustomUser(result);
        customUser.setUserId(user.getId());
        return customUser;
    }

}
