package ru.tsc.ichaplygina.taskmanager.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.ichaplygina.taskmanager.client.rest.ProjectsRestEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

public class ProjectRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/project/";

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final Project project1 = new Project("Test Project 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2");

    @NotNull
    private final Project project3= new Project("Test Project 3");

    @NotNull
    private final Project project4 = new Project("Test Project 4");

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<Project> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Project.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, header));
    }

    @After
    public void clean() {
        @NotNull final String url = PROJECTS_URL + "removeAll/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/auth/logout";
        sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String expected = project4.getName();
        @NotNull final String url = BASE_URL + "add/";
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(project4, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Project project = response.getBody();
        Assert.assertNotNull(project);
        @NotNull final String actual = project.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String expected = project1.getId();
        @NotNull final String url = BASE_URL + "findById/" + expected;
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final Project project = response.getBody();
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        @NotNull final String id = project1.getId();
        @NotNull final String url = BASE_URL + "removeById/" + id;
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        @NotNull final String urlFind = BASE_URL + "findById/" + id;
        Assert.assertNull(sendRequest(urlFind, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final String url = BASE_URL + "save/";
        @NotNull final ResponseEntity<Project> response = sendRequest(url, HttpMethod.PUT, new HttpEntity<>(project4, header));
        @NotNull final String urlFind = BASE_URL + "findById/";
        Assert.assertNull(sendRequest(urlFind + project4.getId(), HttpMethod.GET, new HttpEntity<>(header)).getBody());
        @NotNull final String expected = "Test Project One";
        project1.setName(expected);
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(project1, header));
        @NotNull final String actual = sendRequest(urlFind + project1.getId(), HttpMethod.GET, new HttpEntity<>(header)).getBody().getName();
        Assert.assertEquals(expected, actual);
    }

}
