package ru.tsc.ichaplygina.taskmanager.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.TaskEndpoint;
import ru.tsc.ichaplygina.taskmanager.api.TasksEndpoint;
import ru.tsc.ichaplygina.taskmanager.client.soap.AuthSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.TaskSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.client.soap.TasksSoapEndpointClient;
import ru.tsc.ichaplygina.taskmanager.marker.IntegrationCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class TaskSoapEndpointTest {

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static TaskEndpoint taskEndpoint;

    @NotNull
    private static TasksEndpoint tasksEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final Task task1 = new Task("Test Task 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2");

    @NotNull
    private final Task task3 = new Task("Test Task 3");

    @NotNull
    private final Task task4 = new Task("Test Task 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("user", "user").isSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        tasksEndpoint = TasksSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull final BindingProvider tasksBindingProvider = (BindingProvider) tasksEndpoint;
        Map<String, List<String>> headers= CastUtils.cast((Map)authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        tasksBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        taskEndpoint.add(task1);
        taskEndpoint.add(task2);
        taskEndpoint.add(task3);
    }

    @After
    @SneakyThrows
    public void clean() {
        tasksEndpoint.removeAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testAdd() {
        @NotNull final String expected = task4.getName();
        @Nullable final Task task = taskEndpoint.add(task4);
        Assert.assertNotNull(task);
        @NotNull final String actual = task.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String expected = task1.getId();
        @Nullable final Task task = taskEndpoint.findById(expected);
        Assert.assertNotNull(task);
        final String actual = task.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        @NotNull final String id = task1.getId();
        taskEndpoint.removeById(id);
        Assert.assertNull(taskEndpoint.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        taskEndpoint.save(task4);
        Assert.assertNull(taskEndpoint.findById(task4.getId()));
        @NotNull final String expected = "Test Task One";
        task1.setName(expected);
        taskEndpoint.save(task1);
        @NotNull final String actual = taskEndpoint.findById(task1.getId()).getName();
        Assert.assertEquals(expected, actual);
    }

}
